# -*- coding: utf-8 -*-
from qgis.core import QgsMapLayer

# initialize Qt resources from file resources.py
import resources


DEBUG = True
#pyqtRemoveInputHook()

import os
from os.path import expanduser
import sys
import datetime
from collections import OrderedDict as odict

import platform
from sys import stderr, stdout
import subprocess
import commands
import shutil
import binascii
import tempfile
import ctypes
import tempfile
import re
import fnmatch
import ipdb


try:
    from grass.pygrass.gis import Location, Mapset
except ImportError:
    print('Python GRASS libraries not in the path')
    sys.exit(1)

STDERR = sys.stderr

_modname = os.environ.setdefault('QT_API', 'pyqt')
assert _modname in ('pyqt', 'pyside')

if os.environ['QT_API'] == 'pyqt':
    try:
        import PyQt4  # analysis:ignore
    except ImportError:
        # Switching to PySide
        os.environ['QT_API'] = _modname = 'pyside'

from PyQt4.QtWebKit import *

if os.environ['QT_API'] == 'pyqt':
    try:
        from PyQt4 import QtGui, QtCore
        from PyQt4.QtGui import QFormLayout

    except ImportError:
        raise ImportError("formlayout requires PyQt4 4.4+ (or PySide)")
    from PyQt4.QtGui import (QWidget, QLineEdit, QComboBox, QLabel, QSpinBox,
                             QIcon, QStyle, QDialogButtonBox, QHBoxLayout,
                             QVBoxLayout, QDialog, QColor, QPushButton,
                             QCheckBox, QColorDialog, QPixmap, QTabWidget,
                             QApplication, QStackedWidget, QDateEdit, QPlainTextEdit,
                             QDateTimeEdit, QFont, QFontComboBox,
                             QFontDatabase, QGridLayout, QTextEdit,
                             QDoubleValidator, QAction, QMessageBox, QFileDialog)
    from PyQt4.QtCore import Qt, SIGNAL, SLOT, QSize, QObject, QUrl
    from PyQt4.QtCore import pyqtSlot as Slot
    from PyQt4.QtCore import pyqtProperty as Property

if os.environ['QT_API'] == 'pyside':
    from PySide.QtGui import (QWidget, QLineEdit, QComboBox, QLabel, QSpinBox,
                              QIcon, QStyle, QDialogButtonBox, QHBoxLayout,
                              QVBoxLayout, QDialog, QColor, QPushButton,
                              QCheckBox, QColorDialog, QPixmap, QTabWidget,
                              QApplication, QStackedWidget, QDateEdit, QPlainTextEdit,QWebView,
                              QDateTimeEdit, QFont, QFontComboBox,
                              QFontDatabase, QGridLayout, QTextEdit,
                              QDoubleValidator, QFormLayout, 
                              QAction, QMessageBox, QFileDialog)
    from PySide.QtCore import (Qt, SIGNAL, SLOT, QSize, Slot, Property, 
                               QObject, QUrl)


# ----+- Python 3 compatibility -+----
PY2 = sys.version[0] == '2'

if PY2:
    # Python 2
    import codecs

    def u(obj):
        """Make unicode object"""
        return codecs.unicode_escape_decode(obj)[0]
else:
    # Python 3
    def u(obj):
        """Return string as it is"""
        return obj


###########

grass7path = r'C:\OSGeo4W\apps\grass\grass-7.1.svn'
grass7bin_win = r'C:\OSGeo4W\bin\grass71svn.bat'
# Linux
grass7bin_lin = 'grass71'
# MacOSX
grass7bin_mac = '/Applications/GRASS/GRASS-7.1.app/'
#myepsg = '4326' # latlong
myepsg = '32632' # ETRS-TM32, http://spatialreference.org/ref/epsg/3044/



########### SOFTWARE
if sys.platform.startswith('linux'):
    # we assume that the GRASS GIS start script is available and in the PATH
    # query GRASS 7 itself for its GISBASE
    grass7bin = grass7bin_lin
elif sys.platform.startswith('win'):
    grass7bin = grass7bin_win
else:
    OSError('Platform not configured.')
 
startcmd = grass7bin + ' --config path'
 
p = subprocess.Popen(startcmd, shell=True, 
                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out, err = p.communicate()
if p.returncode != 0:
    #print >>sys.stderr, 'ERROR: %s' % err
    #print >>sys.stderr, "ERROR: Cannot find GRASS GIS 7 start script (%s)" % startcmd
    sys.exit(-1)
home = expanduser("~")

if sys.platform.startswith('linux'):
    gisbase = out.strip('\n')
elif sys.platform.startswith('win'):
    if out.find("OSGEO4W home is") != -1:
        gisbase = out.strip().split('\n')[1]
    else:
        gisbase = out.strip('\n')
    os.environ['GRASS_SH'] = os.path.join(gisbase, 'msys', 'bin', 'sh.exe')
 
# Set GISBASE environment variable
os.environ['GISBASE'] = gisbase

# the following not needed with trunk
os.environ['PATH'] += os.pathsep + os.path.join(gisbase, 'extrabin')
os.environ['PATH'] += os.pathsep + os.path.join(home, '.grass7', 'addons', 'scripts')

# define GRASS-Python environment
gpydir = os.path.join(gisbase, "etc", "python")
sys.path.append(gpydir)
########
# define GRASS DATABASE

"""
if sys.platform.startswith('win'):
    gisdb = os.path.join(os.getenv('APPDATA', 'grassdata')
else:
    gisdb = os.path.join(os.getenv('HOME', 'grassdata')
""" 
# override for now with TEMP dir
gisdb = os.path.join(tempfile.gettempdir(), 'grassdata')
try:
    os.stat(gisdb)
except:
    os.mkdir(gisdb)
 
#ipdb.set_trace() 
# location/mapset: use random names for batch jobs
string_length = 16
location = binascii.hexlify(os.urandom(string_length))
mapset   = 'PERMANENT'
location_path = os.path.join(gisdb, location)
 
# Create new location (we assume that grass7bin is in the PATH)
#  from EPSG code:
startcmd = grass7bin + ' -c epsg:' + myepsg + ' -e ' + location_path

p = subprocess.Popen(startcmd, shell=True, 
                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out, err = p.communicate()
#if p.returncode != 0:
    #print >>sys.stderr, 'ERROR: %s' % err
    #print >>sys.stderr, 'ERROR: Cannot generate location (%s)' % startcmd
    #sys.exit(-1)
#else:
    #print 'Created location %s' % location_path
 
# Now the location with PERMANENT mapset exists.
 
########
# Now we can use PyGRASS or GRASS Scripting library etc. after 
# having started the session with gsetup.init() etc
 
# Set GISDBASE environment variable
os.environ['GISDBASE'] = gisdb
 
# Linux: Set path to GRASS libs (TODO: NEEDED?)
path = os.getenv('LD_LIBRARY_PATH')
dir  = os.path.join(gisbase, 'lib')
if path:
    path = dir + os.pathsep + path
else:
    path = dir
os.environ['LD_LIBRARY_PATH'] = path
 
# language
os.environ['LANG'] = 'en_US'
os.environ['LOCALE'] = 'C'


## Import GRASS Python bindings
import grass.script as gscript
import grass.script.setup as gsetup
from grass.script.core import run_command, parser, overwrite, read_command, parse_command

ow=overwrite()


def is_text_string(obj):
    """Return True if `obj` is a text string, False if it is anything else,
    like binary data (Python 3) or QString (Python 2, PyQt API #1)"""
    if PY2:
        # Python 2
        return isinstance(obj, basestring)
    else:
        # Python 3
        return isinstance(obj, str)


def is_binary_string(obj):
    """Return True if `obj` is a binary string, False if it is anything else"""
    if PY2:
        # Python 2
        return isinstance(obj, str)
    else:
        # Python 3
        return isinstance(obj, bytes)


def is_string(obj):
    """Return True if `obj` is a text or binary Python string object,
    False if it is anything else, like a QString (Python 2, PyQt API #1)"""
    return is_text_string(obj) or is_binary_string(obj)


def to_text_string(obj, encoding=None):
    """Convert `obj` to (unicode) text string"""
    if PY2:
        # Python 2
        if encoding is None:
            return unicode(obj)
        else:
            return unicode(obj, encoding)
    else:
        # Python 3
        if encoding is None:
            return str(obj)
        elif isinstance(obj, str):
            # In case this function is not used properly, this could happen
            return obj
        else:
            return str(obj, encoding)


def is_edit_valid(edit):
    text = edit.text()
    state, _t = edit.validator().validate(text, 0)
    return state == QDoubleValidator.Acceptable



def medit(module, canvas, title="", comment="", icon=None, parent=None, apply=None):
    """Create module dialog and return result"""
    # Create a QApplication instance if no instance currently exists
    # (e.g. if the module is used directly from the interpreter)
    if QApplication.startingUp():
        _app = QApplication([])

    dialog = ModuleDialog(module, canvas, title, comment, icon, parent, apply)
    if dialog.exec_():
        return dialog.get()


class MCheckBox(QCheckBox):
    def text(self):
        if self.isChecked():
            return True
        else:
            return False



class MComboBox(QComboBox):
    def text(self):
        return self.currentText()

class List():
    def __init__(self, name, gtype, parent=None):
        self.gtype = gtype
        self.parent = parent
        self.items = []

    # def init(self):
    #     location = Location()
    #     # mapset = Mapset()
    #     mloc = List(location.name, gtype)
    #     mloc.tooltip = location.path()
    #     for ms in sorted(location.mapsets()):
    #         mset = List(ms, self.gtype, mloc)
    #         mloc.items.append(mset)
    #         for mp in sorted(location[ms].glist(type=self.gtype)):
    #             dt = List(mp, self.gtype, mset)
    #             mloc.child.append(dt)
    #     return mloc

class GListLayout():
    pass


class RasterLayout():
    pass

class ColComboBox(QComboBox):
    """docstring for ColComboBox"""
    def __init__(self, vname,canvas,vchoiche):
        super(ColComboBox, self).__init__()
        self.vname = vname
        self.canvas=canvas
        self.vchoiche=vchoiche
        allLayers = self.canvas.layers()
        layers=dict()
        for layer in allLayers:
            layers[layer.name()]=layer

        vfields = layers[vchoiche].pendingFields()

        for field in vfields:
            self.addItem(field.name)

        


class ModuleWidget(QWidget):
    def __init__(self, plist, params, canvas, comment, parent=None,required=0):
        QWidget.__init__(self, parent)
        #from copy import deepcopy
        #self.mod = module #deepcopy(module)
        self.canvas=canvas
        self.plist=plist
        self.params = params
        self.widgets = []
        self.formlayout = QFormLayout(self)
        self.formlayout.addRow(QLabel(comment))
        self.formlayout.addRow(QLabel(" "))

        self.allLayers = self.canvas.layers()

        self.required=required

        """
        combo = QtGui.QComboBox(self)
        self.allLayers = self.canvas.layers()
        self.listalayers=dict()
        elementovuoto="No required"
        for i in self.allLayers:
            self.listalayers[i.name()]=i
            if i.type() == QgsMapLayer.VectorLayer:            
                combo.addItem(str(i.name()))
            if i.type() == QgsMapLayer.RasterLayer:
                #add item to combobox
                pass


        self.formlayout.addWidget(combo)
        """


        # if DEBUG:
        #     print("\n"+("*"*80))
        #     print("Module:", self.mod)
        #     print("*"*80)

    def get_dialog(self):
        """Return FormDialog instance"""
        dialog = self.parent()
        while not isinstance(dialog, QDialog):
            dialog = dialog.parent()
        return dialog

    def setup(self):
        
        # if DEBUG:
        #     print("setup:", self.mod.name)
        for param in self.params:
            field = self.get_field(param)
            if field is not None:
                #ipdb.set_trace()
                #print str(param.name)+" "+str(param.type)
                self.formlayout.addRow(param.name,field)
                self.widgets.append(field)

        if self.required==1:
            #ipdb.set_trace()
            hbox = QHBoxLayout()
            labelfield = QtGui.QLabel("Saving folder",self)
            field=QLineEdit('', self)
            field.setObjectName("linefolder")
            hbox.addWidget(labelfield)
            hbox.addWidget(field)
            buttonsavefolder = QtGui.QPushButton('Choice', self)
            hbox.addWidget(buttonsavefolder)
            self.formlayout.addRow(hbox)
            self.widgets.append(field)

            hbox = QHBoxLayout()
            labelfield = QtGui.QLabel("Working resolution ",self)
            hbox.addWidget(labelfield)
            field = QSpinBox(self)
            field.setRange(1, 100)
            field.setValue(5)
            hbox.addWidget(field)
            self.formlayout.addRow(hbox)
            self.widgets.append(field)


            #ipdb.set_trace()

            # save_widget=QtGui.QWidget()
            # field=QLineEdit('prova', self)
            # hbox = QHBoxLayout()
            # hbox.addWidget(save_widget)
            # button = QtGui.QPushButton('Test', self)
            # hbox.addWidget(button)




        #ipdb.set_trace()
        # for flag in self.mod.flags:
        #     field = QCheckBox(self)
        #     field.setCheckState(Qt.Unchecked)
        #     self.formlayout.addRow(flag, field)
        #     self.widgets.append(field)

    def get(self):
        for field, param in zip(self.widgets, self.params):
            value = str(field.text())
            param.value = None if value == '' or value == 'False' else value

    def get_field(self, param):
        field = None
        value = param.value if param.value is not None else ''
        if hasattr(param, 'values') and param.values is not None:
            values = list(param.values)
            #selindex = value.pop(0)
            field = MComboBox(self)
            field.addItems(values)
            #if selindex in value:
            #    selindex = value.index(selindex)
            #import ipdb; ipdb.set_trace()
            if param.default is not None:
                if param.default in values:
                    index = values.index(param.default)
                    field.setCurrentIndex(index)
        #ipdb.set_trace()
        if param.typedesc == 'raster':
            if param.input:
                field = MComboBox(self)
                field.setObjectName(param.name)
                listlayers=dict()
                emptyelement=""
                if param.required==False:
                    field.addItem(emptyelement)
                for i in self.allLayers:
                    listlayers[i.name()]=i
                    if i.type() == QgsMapLayer.RasterLayer:            
                        field.addItem(str(i.name()))
            else:
                field = QLineEdit(value, self)
                field.setObjectName(param.name)
        elif param.typedesc == 'vector':
            if param.input:
                field = MComboBox(self)
                field.setObjectName(param.name)
                listlayers=dict()
                emptyelement=""
                if param.required==False:
                    field.addItem(emptyelement)
                for i in self.allLayers:
                    listlayers[i.name()]=i
                    if i.type() == QgsMapLayer.VectorLayer:            
                        field.addItem(str(i.name()))
            else:
                field = QLineEdit(value, self)
                field.setObjectName(param.name)
        elif param.typedesc in ('string', 'file'):
            #ipdb.set_trace()
            plist=param.name.split('_')
            
            if plist[0] in self.plist:
                try:
                    plist[1] == "column"
                    # vbase=self.formlayout.findChild(MComboBox,plist[0])
                    # vchoiche=vbase.currentText()
                    # field = ColComboBox(plist[0],self.canvas,vchoiche) 
                    field = MComboBox(self)
                except:
                    field = QLineEdit(value, self)
            else:
                field = QLineEdit(value, self)
            field.setObjectName(param.name)
        elif param.type == 'boolean':
            field = MCheckBox(self)
            #field.setCheckState(Qt.Unchecked)
        elif param.type == 'integer':
            field = QSpinBox(self)
            if param.isrange:
                field.setRange(param.min, param.max)
            else:
                field.setRange(-1e9, 1e9)
            field.setValue(value)
        elif param.type in (float, 'float', 'double'):
            field = QLineEdit(str(value), self)
            field.setValidator(QDoubleValidator(field))
            dialog = self.get_dialog()
            dialog.register_float_field(field)
            # self.connect(field, SIGNAL('textChanged(QString)'),
            #              lambda text: dialog.update_buttons())
#        elif param.type == 'file':
#            pass
#        elif param.type == 'raster':
#            pass
#        elif param.type == 'vector':
#            pass
        else:
            if DEBUG:
                print('Parameter type not supported!')
                print(param.type)
            return QLineEdit('', self)

        return field




class ModuleTabWidget(QWidget):
    def __init__(self, module, canvas, comment="", parent=None):
        QWidget.__init__(self, parent)
        self.module = module
        self.params= self.module.params_list
        for flag in self.module.flags.values():
            flag.type = 'boolean'
            flag.typedesc = 'boolean'
            self.params.append(flag)
            if flag.guisection is None:
                flag.guisection = 'Options'

        layout = QVBoxLayout()
        self.tabwidget = QTabWidget()
        self.tabwidget.setMinimumSize(500,300)
        layout.addWidget(self.tabwidget)
        self.setLayout(layout)
        self.widgetlist = []
        guititles = [param.guisection for param in self.params 
                     if param.guisection is not None and param not in self.module.required]
        tabs = odict([(gtitle, []) for gtitle in guititles])
        #plist=[]
        plist=dict()
        for param in self.module.params_list:
            if param.name not in self.module.required and param.guisection is not None:
                tabs[param.guisection].append(param)
            #plist.append(param.name)
            plist[param.name]=param.value
        required = [param for param in self.module.params_list if param.name in self.module.required]
        #ipdb.set_trace()
        widget = ModuleWidget(plist, required, canvas, comment=comment, parent=self,required=1)
        index = self.tabwidget.addTab(widget, "Required")
        self.tabwidget.setTabToolTip(index, comment)
        self.widgetlist.append(widget)

        for title, pars in tabs.items():
            widget = ModuleWidget(plist, pars, canvas, comment=comment, parent=self)
            index = self.tabwidget.addTab(widget, title)
            self.tabwidget.setTabToolTip(index, comment)
            self.widgetlist.append(widget)

        #ipdb.set_trace()
        # add output qwebview tab
        tabweb_widget=QtGui.QWidget()
        weboutput=QWebView()
        address_web="http://grass.osgeo.org/grass70/manuals/addons/"+self.module.name+".html"
        address_local=home+"/.grass7/addons/docs/html/"+self.module.name+".html"
        if os.path.isfile(address_local)==True:
            weboutput.load(QUrl(address_local))
        else:
            weboutput.load(QUrl(address_web))
        #weboutput.load(QUrl("http://grass.osgeo.org/grass70/manuals/addons/r.green.biomassfor.legal.html"))
        weboutput.setObjectName("help_view")
        vbox = QVBoxLayout()
        vbox.addWidget(weboutput)
        tabweb_widget.setLayout(vbox)
        self.tabwidget.addTab(tabweb_widget,"Manual")

        # add output textedit tab
        tabout_widget=QtGui.QWidget()
        textoutput = QPlainTextEdit()
        vbox = QVBoxLayout()
        vbox.addWidget(textoutput)
        tabout_widget.setLayout(vbox)
        self.tabwidget.addTab(tabout_widget,"Output")
       
    def setup(self):
        for widget in self.widgetlist:
            widget.setup()
      
    def get(self):
        for widget in self.widgetlist:
            widget.get()
        return self.module


class ModuleDialog(QDialog):
    """Module Dialog"""
    def __init__(self, module, canvas, title="", comment="",
                 icon=None, parent=None, apply=None):
        QDialog.__init__(self, parent)

        # Destroying the C++ object right after closing the dialog box,
        # otherwise it may be garbage-collected in another QThread
        # (e.g. the editor's analysis thread in Spyder), thus leading to
        # a segmentation fault on UNIX or an application crash on Windows
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.module = module
        self.canvas=canvas

        self.apply_callback = apply

        self.params=module.params_list

        self.module.stdout_ = subprocess.PIPE
        self.module.stderr_ = subprocess.PIPE

        self.allLayers = self.canvas.layers()
        self.layers=dict()

        for layer in self.allLayers:
            self.layers[layer.name()]=layer

        # Form
        self.formwidget = ModuleTabWidget(self.module, self.canvas, comment=comment,
                                         parent=self)
        layout = QVBoxLayout()
        layout.addWidget(self.formwidget)

        self.float_fields = []
        self.formwidget.setup()

        # Button box
        self.bbox = bbox = QDialogButtonBox(QDialogButtonBox.Ok
                                            |QDialogButtonBox.Cancel|QDialogButtonBox.Reset)
        # self.connect(self.formwidget, SIGNAL('update_buttons()'),
        #              self.update_buttons)
        # if self.apply_callback is not None:
        #     apply_btn = bbox.addButton(QDialogButtonBox.Apply)
        #     self.connect(apply_btn, SIGNAL("clicked()"), self.apply)
        self.connect(bbox, SIGNAL("rejected()"), SLOT("reject()"))
        self.bbox.accepted.connect(self.run)
        self.bbox.button(QtGui.QDialogButtonBox.Reset).clicked.connect(self.resetcombo)
        layout.addWidget(bbox)

        self.buttonsavefolder=self.formwidget.findChild(QPushButton)

        self.buttonsavefolder.clicked.connect(self.choicefolder)

        self.listCombo=self.formwidget.findChildren(MComboBox)

        self.setLayout(layout)

        self.setWindowTitle(title)
        if not isinstance(icon, QIcon):
            icon = QWidget().style().standardIcon(QStyle.SP_MessageBoxQuestion)
        self.setWindowIcon(icon)

    def register_float_field(self, field):
        self.float_fields.append(field)

    def update_buttons(self):
        valid = True
        for field in self.float_fields:
            if not is_edit_valid(field):
                valid = False
        for btn_type in (QDialogButtonBox.Ok, QDialogButtonBox.Apply):
            btn = self.bbox.button(btn_type)
            if btn is not None:
                btn.setEnabled(valid)

    def accept(self):
        #self.data = self.formwidget.get()
        QDialog.accept(self)

    def reject(self):
        self.data = None
        QDialog.reject(self)

    def apply(self):
        self.apply_callback(self.formwidget.get())

    def get(self):
        """Return form result"""
        # It is import to avoid accessing Qt C++ object as it has probably
        # already been destroyed, due to the Qt.WA_DeleteOnClose attribute
        return self.module.get_python()

    def resetcombo(self):
        #ipdb.set_trace()
        for combo in self.listCombo:
            comboname=combo.objectName()
            combosplit=comboname.split('_')
            try:
                if combosplit[1]=="column":
                    combo.clear()
                    mainvect=self.formwidget.findChild(MComboBox,combosplit[0]).currentText()
                    vfields = self.layers[mainvect].pendingFields()
                    emptyelement=""
                    combo.addItem(emptyelement)
                    for field in vfields:
                        combo.addItem(field.name())
            except:
                pass



    def choicefolder(self):
        self.pathtofolder = QFileDialog.getExistingDirectory(None,"Open a folder","/",QFileDialog.ShowDirsOnly)
        self.linefolder=self.formwidget.findChild(QLineEdit,"linefolder")
        self.linefolder.setText(self.pathtofolder)

    def run(self):
        """Return form result"""
        self.linefolder=self.formwidget.findChild(QLineEdit,"linefolder")
        if self.linefolder.text()=="":
            QMessageBox.warning(self.formwidget,"Warning", "Check prefix output data *" )
            return


        self.formwidget.tabwidget.setCurrentIndex(self.formwidget.tabwidget.count()-1)

        self.output=self.formwidget.findChild(QPlainTextEdit)

        self.spinres=self.formwidget.findChild(QSpinBox)
        res=int(self.spinres.text())

        #ipdb.set_trace()

        self.output.appendPlainText("*** "+self.module.name+" ***")
        self.output.appendPlainText("Description: "+self.module.description)


        self.formwidget.get()

        #ipdb.set_trace()

        for imap in self.module.inputs.values():
            if imap.typedesc=='vector':
                try:
                    vector_layer=self.layers[imap.value]
                    vector_path=vector_layer.dataProvider().dataSourceUri().split('|')
                    run_command("v.in.ogr", input=vector_path[0], output=imap.value,overwrite=True, flags = 'o')
                    if imap.name=='boundaries':
                        run_command("g.region",vect=imap.value,res=res)
                except:
                    pass 
            if imap.typedesc=='raster':    
                try:         
                    raster_layer=self.layers[imap.value]
                    raster_path=raster_layer.dataProvider().dataSourceUri().split('|')
                    run_command("r.in.gdal", input=raster_path[0], output=imap.value,overwrite=True,flags = 'o')
                    if imap.name=='discharge':
                        run_command("g.region", raster=imap.value)
                except:
                    pass

        self.module.run()
        
        mapprodr=parse_command('g.list', type='raster').keys()
        mapprodv=parse_command('g.list', type='vector').keys()

        for omap in self.module.outputs.values():
            #ipdb.set_trace()
            if fnmatch.fnmatch(omap.name,'output_basename*') and omap.value:
                for map in mapprodr:  
                    if fnmatch.fnmatch(map,omap.value+'*'):               
                        run_command("r.out.gdal", input=map, output=self.pathtofolder+"/"+map+".tif",overwrite=True,format = 'GTiff')    
                for map in mapprodv:
                    if fnmatch.fnmatch(map,omap.value+'*'):                  
                        run_command("v.out.ogr", input=map, output=self.pathtofolder+"/"+map,overwrite=True,format = 'ESRI_Shapefile')     

        self.output.appendPlainText("\n")
        self.output.appendPlainText(self.module.outputs.stdout)
        self.output.appendPlainText("\n")

        return self.module.run()




class R_green:
    def __init__(self, iface, icon, menu):
        # save reference to the QGIS interface
        self.iface = iface
        self.icon = icon
        self.menu = menu
        self.actions = odict()
    
    def initGui(self):
        for mtitle, modules in self.menu:
            for module in modules:
                # create action that will start plugin configuration
                action = QAction(QIcon(self.icon), 
                                 module, self.iface.mainWindow())
                action.setObjectName(module)
                QObject.connect(action, SIGNAL("triggered()"), 
                                self.get_run(module))
                # add toolbar button and menu item
                self.iface.addPluginToMenu(mtitle, action)
                # store the action
                self.actions[module] = action

        # connect to signal renderComplete which is emitted when canvas
        # rendering is done
        QObject.connect(self.iface.mapCanvas(), SIGNAL("renderComplete(QPainter *)"), self.renderTest)
        self.canvas=self.iface.mapCanvas()

    def unload(self):
        #for module, action in self.actions:
            # TODO: finish
            # remove the plugin menu item and icon
        #    self.iface.removePluginMenu("&R.green", self.action_legal)
        #    self.iface.removeToolBarIcon(self.action_legal)

        # disconnect form signal of the canvas
        QObject.disconnect(self.iface.mapCanvas(), SIGNAL("renderComplete(QPainter *)"), self.renderTest)


    def get_run(self, module_name):
        def run():
            def apply_test(data):
                print("data:", data)

            from grass.pygrass.modules import Module

            gsetup.init(gisbase, gisdb, location, mapset)
            module = Module(module_name)
            print("result:", medit(module, self.canvas, title=module_name.title(),
                                   comment="Insert mandatory parameters",
                                   apply=apply_test))
        return run

    def renderTest(self, painter):
        # use painter for drawing to map canvas
        print "TestPlugin: renderTest called!"

