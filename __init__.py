def classFactory(iface):
    from mainPlugin import R_green
    #from PyQt4.QtCore import *
    #pyqtRemoveInputHook()
    #import ipdb; ipdb.set_trace()
    menu = [('r.green.biomassfor', ('r.green.biomassfor.theoretical',
                                    'r.green.biomassfor.legal',
                                    'r.green.biomassfor.technical',
                                    'r.green.biomassfor.economic',
                                    'r.green.biomassfor.co2', ), ),
            ('r.green.hydro',       ('r.green.hydro.recommended', ), ),
                                    #'r.green.hydro.theoretical',
                                    #'r.green.hydro.theoretical',
                                    #'r.green.hydro.theoretical', ), ),
            ]
    return R_green(iface, icon=":/plugins/rgreen/icon.png", menu=menu)

## any other initialisation needed
